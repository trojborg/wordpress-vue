<?php

/**
 * Application routes.
 */
Route::get('/', function () {
    return view('pages.front');
});

Route::any('page', function ($post, $query) {
    return view('pages.default', [
        'page' => $post, // not required
    ]);
});

Route::any('singular', function ($post, $query) {

    return view('blog.single', [
        'post' => $post, // not required
    ]);
});