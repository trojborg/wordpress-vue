<?php

namespace Cetro\Standapplone\Providers;

use Illuminate\Support\ServiceProvider;
use Themosis\Support\Facades\Asset;

class AssetServiceProvider extends ServiceProvider
{
    protected $plugin;

    public function register()
    {
        $this->plugin = $this->app->make('wp.plugin.standapplone');
        $this->enqueueScripts();
        $this->enqueueStyles();
        // Asset::add('plugin_styles', 'css/style.css', [], $plugin->getHeader('version'))->to('front');
        // Asset::add('plugin_js', 'js/index.min.js', [], $plugin->getHeader('version'))->to('front');
    }

    private function isDevServe()
    {
        $connection = @fsockopen('localhost', '8080');

        if ( $connection ) {
            return true;
        }

        return true;
    }

    public function enqueueScripts() {
        $plugin_version = $this->plugin->getHeader('version');
        $vue_directory = $this->plugin->getUrl() . '/vueapp/dist';

        if ( $this->isDevServe() ) {
            Asset::add(
                $this->plugin->getHeader('name') . '_dev',
                $vue_directory . '/app.js',
                [],
                $plugin_version
            )->to('front');
        } else {
            Asset::add(
                $this->plugin->getHeader('name') . '_chunks',
                plugin_dir_url( __DIR__ ) . 'dist/js/chunk-vendors.js',
                [],
                $plugin_version
            )->to('front');

            Asset::add(
                $this->plugin->getHeader('name'),
                plugin_dir_url( __DIR__ ) . 'dist/js/app.js',
                [],
                $plugin_version
            )->to('front');

        }

    }
    public function enqueueStyles() {

        $plugin_version
            = $this->plugin->getHeader('version');


        $vue_directory = $this->plugin->getUrl() . '/vueapp/dist';


        if ( $this->isDevServe() ) {

            Asset::add(
                $this->plugin->getHeader('name') . '_dev',
                $vue_directory . '/app.css',
                [],
                $plugin_version,
                'all'
            )->to('front');

        } else {
            Asset::add(
                $this->plugin->getHeader('name') . '_dev',
                plugin_dir_url( __DIR__ ) . 'dist/css/app.css',
                [],
                $plugin_version,
                'all'
            )->to('front');
        }
    }
}