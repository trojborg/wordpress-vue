const devPort = 8081;
module.exports = {
    devServer: {
        hot: true,
        writeToDisk: true,
        liveReload: false,
        sockPort: devPort,
        port: devPort,
        headers: { "Access-Control-Allow-Origin": "*" },
        disableHostCheck: true,
    },
    publicPath:
        process.env.NODE_ENV === "production"
            ? '/content/mu-plugins/standapplone/dist/'
            : `http://localhost:${devPort}/ `,
    configureWebpack: {
        output: {
            filename: "app.js",
            hotUpdateChunkFilename: "hot/hot-update.js",
            hotUpdateMainFilename: "hot/hot-update.json"
        },
        optimization: {
            splitChunks: false
        }
    },
    filenameHashing: true,
    css: {
        extract: {
            filename: "app.css"
        },
        loaderOptions: {
            scss: {
                prependData: `@import "~@/assets/styles/main.scss";`
            },

        }
    }
};
